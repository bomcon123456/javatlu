class Employee
{
    private String m_FirstName;
    private String m_LastName;
    private double m_Salary;

    Employee(String fn, String ln, double sl)
    {
        m_FirstName = fn;
        m_LastName = ln;
        m_Salary = sl;
    } 

    public void SetFN(String fn)
    {
        m_FirstName = fn;
    }

    public void SetLN(String ln)
    {
        m_LastName = ln;
    }

    public void SetSalary(double s)
    {
        if(s<0.0)
        {
            m_Salary = 0;
        }
        m_Salary = s;
    }   

    public String GetFN()
    {
        return m_FirstName;
    }

    public String GetLN()
    {
        return m_LastName;
    }

    public double GetMonthlySalary()
    {
        return m_Salary;
    }

    public double GetYearlySalary()
    {
        return (m_Salary * 12);
    }

    public void Print()
    {
        System.out.println("Me name iz: " + m_FirstName + " has: " + GetYearlySalary() + "$ this year.");
    }
}

public class EmployeeTest
{
    public static void main(String[] args)
    {
        Employee e1 = new Employee("Boi", "Mr.", 10.5);
        Employee e2 = new Employee("Gurl", "Mrs.", 15.5);

        e1.Print();
        e2.Print();

        e1.SetSalary(e1.GetMonthlySalary() * 1.1);
        e2.SetSalary(e2.GetMonthlySalary() * 1.1);

        System.out.println();

        e1.Print();
        e2.Print();
    }
}