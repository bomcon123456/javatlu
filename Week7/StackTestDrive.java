import pkg.myStack;

public class StackTestDrive
{
    public static void main(String[] args)
    {
        myStack a = new myStack();
        a.pop();
        // a.push(5);
        // a.push(7);
        // a.pop();
        // a.push(8);
        // a.push(10);
        a.display();
        System.out.println("We found this key at pos: " + a.search(8) + "[0 means can't be found]");
    }
}