// Package Stack so we can use multiple files/folders
package pkg;

// Node class, should not be used directly by the users.
class Node
{
    int item;
    Node next;
    Node()
    {
        item = 0;
        next = null;
    }
    Node(int input)
    {
        item = input;
        next = null;
    }
}

// Stack class.
public class myStack
{
    private Node top;
    int size;
    // Initialize the stack.
    public myStack()
    {
        top = null;
        size = 0;
    }

    // Control elements
    public void push(int input)
    {
        size++;
        if(size == 0)
        {
            top = new Node(input);
            
            return;
        }
        Node temp = new Node(input);
        temp.next = top;
        top = temp;
    }
    public Node pop()
    {
        if(size == 0)
        {
            System.out.println("[POP] Stack has no element.");
            return new Node(-1);
        }
        Node res = top;
        top = top.next;
        size--;
        return res;
    }
    // Control elements

    // Gather information
    public boolean isEmpty()
    {
        if(size == 0)
            return true;
        return false;
    }
    public int numOfElement()
    {
        return size;
    }
    public int search(int key)
    {
        int i = 1;
        Node cur = top;
        while(cur != null)
        {
            if(cur.item == key)
            {
                return i;
            }
            cur = cur.next;
            i++;
        }
        return 0;
    }
    public void display()
    {
        if(size == 0)
        {
            System.out.println("[DISPLAY] Stack has no element.");
            return;
        }
        
        Node cur = top;
        
        while(cur != null)
        {
            System.out.print(cur.item + " ");
            cur = cur.next;
        }
        System.out.println();
    }
    // Gather information

}