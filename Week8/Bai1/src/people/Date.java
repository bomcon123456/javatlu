package people;

public class Date {
	int day;
	int month;
	int year;
	
	Date()
	{
		day = 0;
		month = 0;
		year = 0;
	}
	
	public Date(int d, int m, int y)
	{
		day = d;
		month = m;
		year = y;
	}
	
	Date(Date n)
	{
		day = n.day;
		month = n.month;
		year = n.year;
	}
	
	public String toString()
	{
		return year + "/" + month + "/" + day;
	}
}
