package people;

public class Person {
	private String name;
	private Date birthday;
	
	public Person()
	{
		name = new String();
		birthday = new Date();
	}
	
	public Person(String n, int d, int m, int y)
	{
		name = new String(n);
		birthday = new Date(d,m,y);
	}
	
	public Person(String n, Date d)
	{
		name = new String(n);
		birthday = new Date(d);
	}
	
	
	public String getName()
	{
		return name;
	}
	
	public String toString()
	{
		return "This person name is: "  + name + ". His/her birthday is: " + birthday.toString() ;
	}
}
