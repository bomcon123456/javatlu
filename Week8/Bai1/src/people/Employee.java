package people;

public class Employee extends Person {
	private double salary;
	public Employee()
	{
		super();
		salary = 0.0;
	}
	public Employee(String name, int d, int m, int y, double s)
	{
		super(name, d, m, y);
		salary = s;
	}
	public Employee(String name, Date d, double s)
	{
		super(name, d);
		salary = s;
	}
	
	public double getSalary()
	{
		return salary;
	}
	
	public String toString()
	{
		return "This is a employee having name: " + getName()+ "\n\t\tSalaries: " + getSalary();
	}
}
