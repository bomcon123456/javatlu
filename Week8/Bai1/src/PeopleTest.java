import people.Date;
import people.Employee;
import people.Person;

public class PeopleTest {
	public static void main(String[] args)
	{
		Employee newbie = new Employee("Newbie", new Date(10,2,1989), 1000000);
		
		Manager boss = new Manager("Boss", new Date(23,2,1979), 4000000);
		boss.setAssistant(newbie);

		Manager bigBoss = new Manager("BigBoss", new Date(3,12,1969), 10000000);
		bigBoss.setAssistant(boss);
		
		Person[] array = new Person[3];
		array[0] = newbie;
		array[1] = boss;
		array[2] = bigBoss;
		for(int i = 0; i<3; i++)
		{
			System.out.println(array[i].toString());
		}
	}
}
