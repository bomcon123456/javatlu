package anim;

public class Animal {
	String name;
	public Animal()
	{
		name = "no name";
	}
	public Animal(String n)
	{
		name = n;
	}
	public String getName()
	{
		return name;
	}
	public void sayHello()
	{
		System.out.println("Well... I don't know what to say.");
	}
	public void selfIntroduce()
	{
		String s = getClass().toString();
		int index = s.lastIndexOf(".");
		s = s.substring(index + 1).toLowerCase();
		System.out.println("My name is " + getName() + ". I am a/an " + s);
	}
}
