package anim;

public class Cow extends Animal {

	public Cow() 
	{
		super();
	}

	public Cow(String n) 
	{
		super(n);
	}

	public void sayHello()
	{
		System.out.println("Mooo...");
	}
}
