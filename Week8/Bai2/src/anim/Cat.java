package anim;

public class Cat extends Animal {

	public Cat() 
	{
	}

	public Cat(String n) 
	{
		super(n);
	}

	public void sayHello()
	{
		System.out.println("Meow...");
	}
}
