import anim.*;

public class AnimalTest {

	public static void main(String[] args) {
		Animal a = new Animal();
		a.selfIntroduce();
		Animal b = new Cat();
		b.selfIntroduce();
		Animal c = new Cow();
		c.selfIntroduce();
	}

}
